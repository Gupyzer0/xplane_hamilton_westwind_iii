# XPlane Hamilton Westwind III

This is a project for making a Hamilton Westwind III for xplane.

## What's a Hamilton Westwind III

Some might say "it's a monster !!!" but is just a Beech 18 with a long nose and a pair of PT6A turboprops.

## Things I gotta do

- [ ] Actually learn to use blender
- [ ] Fix current 3d model, the nose needs some fixing, also the dimensions.
- [ ] Install xplane 10 . . .
- [ ] Make the xplane model for the flight dynamics
- [ ] Animate current 3d model
- [ ] Code simple panel and test flight the thing !

### Feel free to fork this (or help me)  !

But give me credit =)